const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_accidentes(message: String): String,
    all_accidentes_covariables(limit: Int!, filter: String!): [CovariableAccidentes!]!
    ocurrence_accidentes_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceAccidentes!]!
  
  }
`;

module.exports = schema;