const ocurrence_type = require('./ocurrence_type');
const covariable_type = require('./covariable_type');

module.exports = [
	ocurrence_type,  
	covariable_type,
];
