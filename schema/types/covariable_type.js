const { gql } = require('apollo-server');

module.exports = gql`
  type CovariableAccidentes implements Covariable @key(fields: "id") {
	id: ID!,
  	name: String!,
  	interval: String,
  	bin: Int!,
  	code: String!,
  	lim_inf: Int!,
  	lim_sup: Int!,
	cells_state: [String!]!,
	cells_mun: [String!]!,
	cells_ageb: [String!]!
  }
`;
