const { gql } = require('apollo-server');

module.exports = gql`
  type OcurrenceAccidentes implements Ocurrence @key(fields: "id") {
	id :ID!,
  	cvegeomeso: Int,
  	nom_meso: String,
  	cvegeoeco: Int,
  	nom_geoeco: String,
  	cvegeocona: Int,
  	nom_conapr: String,
  	cvegeoedo: Int,
  	nom_ent: String,
  	cve_nresun: Int,
  	nom_zmetro: String,
  	cve_mun: Int,
  	nom_mun: String,
  	cvemuncona: Int,
	gridid_state :String,
	gridid_mun :String,
	gridid_ageb :String,
	id_covariable :Int!
  }
`;
