const all_covariables = require('./covariable');
const ocurrence_by_id_covariable = require('./ocurrence');

exports.query_provider = {
  all_covariables: all_covariables,
  ocurrence_by_id_covariable: ocurrence_by_id_covariable,
};