async function ocurrence_by_id_covariable(id_covariable, filter, context) {
  const conn = await context.conn.psql;

  context.logger.log('Llamada a BD all_covariable');

  var filter_str = 'true';

  if(filter != ''){
    filter_str = filter;
  }

  const query = `
    SELECT *
    FROM ocurrence
    WHERE id_covariable = ${id_covariable}
    	AND not gridid_mun is null
      AND ${filter_str}
    `;

  const results = await conn.any(query);
  return results;
}

exports.ocurrence_by_id_covariable = ocurrence_by_id_covariable;