async function all_covariables(limit, filter, context) {
  const conn = await context.conn.psql;
  context.logger.log('Llamada a BD all_covariable');

  var filter_str = 'true';

  if(filter != ''){
    filter_str = filter;
  }

  const query = `
    SELECT *
    FROM covariable
    WHERE ${filter_str}
    LIMIT ${limit}
    `;

  const results = await conn.any(query);
  return results;
}

exports.all_covariables = all_covariables;