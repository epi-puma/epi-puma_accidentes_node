const lodash = require('lodash');
const { query_provider } = require('../data_source');

const resolvers = {
	Query: {
	    ocurrence_accidentes_by_id_covariable: async (parent, {id_covariable, filter}, context) => {
	    	return query_provider.ocurrence_by_id_covariable.ocurrence_by_id_covariable(id_covariable, filter, context);
		},
  	},
};

exports.resolvers = resolvers;
