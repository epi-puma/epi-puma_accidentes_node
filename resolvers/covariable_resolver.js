const lodash = require('lodash');
const { query_provider } = require('../data_source');

const resolvers = {
	Query: {
	    all_accidentes_covariables: async (parent, {limit, filter}, context) => {
	    	return query_provider.all_covariables.all_covariables(limit, filter, context);
		},
  	},
};

exports.resolvers = resolvers;
